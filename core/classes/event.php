<?php

/**
 * Created by PhpStorm.
 * Auther @ Mads Roloff - Rights reservede to Author
 * Date: 19-06-2017
 */
class event
{
    private $db;
    public $iEventID = -1;
    public $vcImage;
    public $vcTitle;
    public $vcShortDesc;
    public $txLongDesc;

    public $daExpireDate;
    public $daStartDate;
    public $vcPlace;
    public $iDonate;
    public $vcDate;
    public $daCreated;
    public $iSuspended;
    public $iDeleted;
    Public $vcImagePath;

    public $arrFormElms = array();
    public $arrLabels = array();
    public $arrColumns = array();
    public $arrValues = array();
    public $unset = array();

    public function __construct()
    {
        global $db;
        $this->db = $db;
        //$this->CreateTable();
        $this->Table = "event";

        $this->arrFormElm = array(
            "iEventID" => array("label" => "", "type" => "hidden", "placeholder" => "", "required" => "data-requried='1'", "dbname" => "iEventID", "Filter" => FILTER_SANITIZE_NUMBER_INT),
            "vcTitle" => array("label" => "Event Titel", "type" => "text", "placeholder" => "Event Titel", "required" => "data-requried='1'", "dbname" => "vcTitle", "Filter" => FILTER_SANITIZE_STRING),
            "vcShortDesc" => array("label" => "Kort Beskrivelse", "type" => "textarea", "placeholder" => "Kort Beskrivelse", "required" => "data-requried='1'", "dbname" => "vcShortDesc", "Filter" => FILTER_DEFAULT),
            "txLongDesc" => array("label" => "Lang Beskrivelse", "type" => "textarea", "placeholder" => "Lang Beskrielvse", "required" => "data-requried='1'", "dbname" => "txLongDesc", "Filter" => FILTER_DEFAULT),
            "vcImage" => array("label" => "", "type" => "hidden", "placeholder" => "Lang Beskrielvse", "required" => "data-requried='1'", "dbname" => "vcImage", "Filter" => FILTER_SANITIZE_STRING),
            "vcPlace" => array("label" => "vcPlace", "type" => "text", "placeholder" => "vcPlace", "required" => "data-requried='1'", "dbname" => "vcPlace", "Filter" => FILTER_SANITIZE_STRING),
            "IDonate" => array("label" => "iDonate", "type" => "text", "placeholder" => "iDonate", "required" => "data-requried='1'", "dbname" => "IDonate", "Filter" => FILTER_SANITIZE_NUMBER_FLOAT),
            "daStartDate" => array("label" => "StartTidspunkt", "type" => "date", "placeholder" => "Vælg Dato", "required" => "data-requried='1'", "dbname" => "daStartDate", "Filter" => FILTER_SANITIZE_NUMBER_INT),
            "daExpireDate" => array("label" => "Slut Tidspunkt", "type" => "date", "placeholder" => "Vælg Dato", "required" => "data-requried='1'", "dbname" => "daExpireDate", "Filter" => FILTER_SANITIZE_NUMBER_INT),

            //"daCreated" => array("label" => "", "type" => "hidden", "placeholder" => "", "required" => "data-requried='1'", "dbname" => "daCreated", "Filter" => FILTER_SANITIZE_NUMBER_INT),

            //Unneeded fields
            //"iSuspended" => array(),
            //"iDeleted" => array(),
            //"iOrgID" => array("label" => "", "type" => "hidden", "placeholder" => "",),
            //"vcImage" => array("label" => "Image", "shpwn", "type" => "text", "require" => "", "placeholder" => "Pick a image",),

        );

    }

    public function GetSelect($limit = 100)
    {
        $sql = "select * from $this->Table WHERE iDeleted = 0 LIMIT $limit";
        return $this->db->_fetch_array($sql, array());
    }

    public function GetList()
    {
        $sql = "SELECT * FROM $this->Table WHERE iDeleted = 0";
        //Shows Column names from "Event"
        $sqlLabels = "SHOW FULL COLUMNS FROM $this->Table";
        $this->arrLabels = $this->db->_fetch_array($sqlLabels, array());
        $this->arrValues = $this->db->_fetch_array($sql, array());

        //UNSET = Columns to avoid in LIST
        $this->unset = array(
            "iEventID",
            "daCreated",
            "iSuspended",
            "iDeleted",

        );
    }

    public function getDetails($id)
    {
        $sql = "SELECT * FROM $this->Table WHERE iDeleted = 0 AND iEventID = ?";
        //Shows Column names from "$this->table"
        $sqlLabels = "SHOW FULL COLUMNS FROM $this->Table";
        $this->arrLabels = $this->db->_fetch_array($sqlLabels, array());
        $this->arrValues = $this->db->_fetch_array($sql, array($id));

        //UNSET = Columns to avoid in LIST EDIT FOR DIFFRENT USE!
        $this->unset = array(
            "iEventID",
            "daCreated",
            "iSuspended",
            "iDeleted",
        );
    }

    /**
     * Get Event
     * change $this->"class for use"
     * Used for edit mode
     */
    public function getEvent($id)
    {
        $this->iEventID = $id;
        $sql = "SELECT * FROM $this->Table WHERE iDeleted = 0 AND $id = ?";
        $row = $this->db->_fetch_array($sql, array($id));
        foreach ($row as $key => $value) {
            $this->$key = $value;
        }
        return $row;
    }

    public function FooterEvent($limit)
    {
        $sql = "SELECT * FROM $this->Table WHERE iDeleted = 0 ORDER BY daCreated DESC LIMIT $limit";
        $row = $this->db->_fetch_array($sql, array());
        foreach ($row[0] as $key => $value) {
            $this->$key = $value;
        }
    }

    public function UpdateEvent($iEventID)
    {
        //unsets IEventID from the array
        unset($this->arrFormElm["iEventID"]);

        //MAkes a foreach that gets "filter" from $arrFormElm
        foreach ($this->arrFormElm as $key => $value) {
            $f[$key] = filter_input(INPUT_POST, $key, $value["Filter"]);
        }
        //uses the value of the "filter"'s and takes the value from the $_POST and inserts them into an array ordered by the "FIlTER"
        $params = array_values($f);

        //iEventID = IEventID
        $this->iEventID = $iEventID;

        //awesome SQL that makes an string that looks like this
        //"UPDATE Event SET vcTitle = ?, vcLastName = ?, vcEventName = ?, vcPassword = ?, vcAddress = ?, iZip = ?, vcCity = ?, vcEmail = ?, vcPhone1 = ?, vcPhone2 = ? WHERE iEventID = 12"
        echo $sql = "UPDATE $this->Table SET " . implode(array_keys($this->arrFormElm), " = ?, ") . " = ? WHERE iEventID = $this->iEventID";
        return $this->db->_query($sql, $params);
    }

    public function DeleteEvent($iEventID)
    {
        $this->iEventID = $iEventID;
        //SQL that sets iDeleted to 1
        $sql = "UPDATE $this->Table SET iDeleted = 1 WHERE iEventID = ?";
        return $this->db->_query($sql, array($iEventID));
    }

    Public function CreateEvent($iEventID)
    {

        //unsets IEventID from the array
        unset($this->arrFormElm["iEventID"]);
        unset($this->arrFormElm["daCreated"]);
        $f = array();
        //Makes a foreach that gets "filter" from $arrFormElm
        foreach ($this->arrFormElm as $key => $value) {
            $f[$key] = isset($_POST[$key]) ? $_POST[$key] : NULL;
            $f[$key] = filter_var($f[$key], $value["Filter"]);
        }

        $f["daStartDate"] = isset($f["daStartDate"]) ? strtotime($f["daStartDate"]) : time();
        $f["daExpireDate"] = isset($f["daExpireDate"]) ? strtotime($f["daExpireDate"]) : time() + (60 * 60 * 30);

        //uses the value of the "filter"'s and takes the value from the $_POST and inserts them into an array ordered by the "FIlTER"
        $params = array_values($f);

        //iEventID = IEventID
        $this->iEventID = $iEventID;

        //awesome SQL that makes an string that looks like this
        //"UPDATE Event SET vcTitle = ?, vcLastName = ?, vcEventName = ?, vcPassword = ?, vcAddress = ?, iZip = ?, vcCity = ?, vcEmail = ?, vcPhone1 = ?, vcPhone2 = ? WHERE iEventID = 12"
        echo $sql = "INSERT INTO $this->Table SET " . implode(array_keys($this->arrFormElm), " = ?, ") . " = ?, daCreated = " . time();
        return $this->db->_query($sql, $params);
    }
}