<?php

/**
 * Created by PhpStorm.
 * Auther @ Mads Roloff - Rights reservede to Author
 * Date: 08-09-2017
 *
 *
 * Calendar class, Call the calendar to get a HTML Calendar printed
 */
class calender
{
    private $db;
    public $arrMonthNames = array(
        1 => "Januar",
        2 => "Februar",
        3 => "Marts",
        4 => "April",
        5 => "Maj",
        6 => "Juni",
        7 => "Juli",
        8 => "August",
        9 => "September",
        10 => "Oktober",
        11 => "November",
        12 => "December"
    );
    public $arrDayNames = array(
        1 => "Mandag",
        2 => "Tirsdag",
        3 => "Onsdag",
        4 => "Torsdag",
        5 => "Fredag",
        6 => "Lørdag",
        7 => "Søndag",
    );
    public $arrBusinessHours = array(
        1 => "8:00",
        2 => "8:30",
        3 => "9:00",
        4 => "9:30",
        5 => "10:00",
        6 => "10:30",
        7 => "11:00",
        8 => "11:30",
        9 => "12:00",
        10 => "12:30",
        11 => "13:00",
        12 => "13:30",
        13 => "14:00",
        14 => "14:30",
        15 => "15:00",
        16 => "15:30",
        17 => "16:00",
        18 => "16:30",
        19 => "17:00",
    );
    public $arrDays = array();

    public $day = 1;
    public $day_next = 1;

    public $month_startday;
    public $month_numdays;
    public $prev_month;
    public $next_month;
    public $cur_month;
    public $numcells;
    public $s;
    Public $Table = "booking";
    public $auth;

    public function __construct($s)
    {
        global $db;
        $this->db = $db;
        $this->CreateTable();
        $this->s = $s;
        $this->cur_month = mktime(0, 0, 0, date("n", $s), 1, date("Y", $s));

        $this->prev_month = mktime(0, 0, 0, date("m", $s) - 1, 1, date("Y", $s));
        $this->next_month = mktime(0, 0, 0, date("m", $s) + 1, 1, date("Y", $s));
        $this->month_numdays = date("t", $this->cur_month);
        /**
         * If you change Month_Sstart day to w sunday will be first as 0 and saturday will be last as 7
         */
        $this->month_startday = date("N", $this->cur_month) - 1;

        $this->numcells = ($this->month_startday + $this->month_numdays);

        $this->numcells = ceil($this->numcells / 7) * 7;

    }

    public function MakeDays()
    {
        for ($i = 0; $i < $this->numcells; $i++) {
            if ($i < $this->month_startday) {
                //Beregner forrige måneds første slutdag i den aktuelle visning
                $numdays_prev = (date("t", $this->prev_month) - $this->month_startday) + 1;
                //Tæller slutdag op med $i
                $day_prev = $numdays_prev + $i;
                //Sætter timestamp for slutdag ud fra variablen prevmonth
                $stamp_prev = mktime(0, 0, 0,
                    date("m", $this->prev_month), $day_prev, date("Y", $this->prev_month));
                //Tilføjer stamp og dato til arrDays
                $this->arrDays[] = array($stamp_prev, $day_prev);
            } else if ($this->day > $this->month_numdays) {
                //Sætter timestamp for dage i næste måned ud fra $nextmonth
                $stamp_next = mktime(0, 0, 0,
                    date("m", $this->next_month), $this->day_next, date("Y", $this->next_month));
                $this->arrDays[] = array($stamp_next, $this->day_next);
                $this->day_next++;
            } else {
                $stamp = mktime(0, 0, 0, date("m", $this->s), $this->day, date("Y", $this->s));
                $this->arrDays[] = array($stamp, $this->day);
                $this->day++;
            }
        }
    }

    /**
     * Make Header, makes the month displays aswell as arrows to move to another month
     */
    public function MakeHeader()
    {
        $acchtml = '<div class="col-lg-8">'
            . '<table style="width: 100% !important;" class="calendar table table-bordered">'
            . '<div style="text-align: center;">'
            . '<a href="?s=' . $this->prev_month . '">&laquo;</a><?php echo "\n" ?>'
            . $this->arrMonthNames[date("n", $this->s)] . " " . date("Y", $this->s)
            . '<a href="?s=' . $this->next_month . '">&raquo;</a>';
        foreach ($this->arrDayNames as $value) {
            $acchtml .= "<td style='background-color: black; color: white;'>" . substr($value, 0, 1) . "</td>";
        }
        $acchtml .= '</div >';
        echo $acchtml;
    }

    public function PrintCalendar()
    {
        $this->MakeDays();
        $this->MakeHeader();
        $acchtml = "";

        foreach ($this->arrDays as $key => $arrValues) {
            //Definer array til CSS klasser for hver celle
            $arrClasses = array();

            //Tilføj klassen today til $arrClasses hvis celle er dags dato
            if (date("d m Y", $arrValues[0]) === date("d m Y")) {
                $arrClasses[] = "today";
            }
            if (date("d m Y", $arrValues[0]) === date("d m Y")) {
                $arrClasses[] = "today";
            }

            //Indsæt klasse til weekend/hverdag efter ugedagens nummer
            $arrClasses[] = (date("N", $arrValues[0]) > 5) ? "weekend" : "everyday";
            //Indsæt klasse til datoer udenfor den pågældende måned

            $arrClasses[] = (date("n", $arrValues[0]) != date("n", $this->s)) ? "dimmed" : "";
            /**
             * Loop arrDays med key og arrValues
             * arrValues[0] = timestamp
             * arrValues[1] = dato
             **/

            //Indsæt row når $key er delelig med 7
            $acchtml .= ($key % 7 === 0) ? "<tr>" : "";

            //Udskriv celle med dato og link til GET parameter med timestamp værdi
            $acchtml .= "<td class=\"" . implode($arrClasses, " ") . "\">";

            $acchtml .= "<a href=\"?mode=BOOKING&s=" . $arrValues[0] . "\">" . $arrValues[1] . "</a>";

            $acchtml .= "</td>\n";
        }

        //Afslut row når $key%7 giver en rest på 6
        $acchtml .= ($key % 7 === 6) ? "</tr>" : "";


        $acchtml .= '</table>'
            . '</div>';

        echo $acchtml;
    }

    public
    function save($iUserID)
    {
        $params = array($_POST["comment"], $_POST["iBookingTime"], 1, $iUserID);
        $sql = "INSERT INTO booking (vcBookingName,iBookingTime,iUserID,iDeleted) VALUES(?,?,?,?)";

        $this->db->_query($sql, $params);
    }

    public function Delete($iBookingID)
    {
        $sql = "DELETE FROM $this->Table WHERE iBookingID=" . $iBookingID;
        $this->db->_query($sql);
    }

    public function GetBookings($iUserID)
    {
        $sql = "Select * FROM $this->Table WHERE iUserID =" . $iUserID;
        return $this->db->_fetch_array($sql);
    }

    public function CheckifSigned()
    {
        $sql = "SELECT iBookingTime FROM $this->Table";
        return $this->db->_fetch_array($sql);
    }

    public function CreateTable()
    {
        $sql = "CREATE TABLE IF NOT EXISTS `booking` (
  `iBookingiD` bigint(20) NOT NULL AUTO_INCREMENT,
  `vcBookingName` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `iBookingTime` bigint(20) NOT NULL DEFAULT '0',
  `iUserID` bigint(20) NOT NULL,
  `iDeleted` tinyint(2) NOT NULL,
  PRIMARY KEY (`iBookingiD`)
) ENGINE=MyISAM AUTO_INCREMENT=68 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";
        $this->db->_query($sql);
    }

}