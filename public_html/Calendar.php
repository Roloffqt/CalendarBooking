<?php
/**
 * Created by PhpStorm.
 * Auther @ Mads Roloff - Rights reservede to Author
 * Date: 08-09-2017
 */
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/init.php";
sysHeader();
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/nav.php";
$mode = setMode();
if ($auth->user->sysadmin || $auth->user->admin) {
    /**
     * Calendar class is used by making $s get on time();
     * then calling Class Calendar with $s as parameter
     * $s : Timestamp
     */
    $s = isset($_GET["s"]) && !empty($_GET["s"]) ? (int)$_GET["s"] : time();
    $calendar = new Calender($s);

    /**
     * These vars are made to make my Seclect field
     * Day start is 8 hours from midnight and will therefor be 8:00
     * Day end is 17 hours from midnight and will therefor be 17:00
     */

//Day start = 8 Hours
    $daystart = 28800;
//Day end = 17 hours
    $dayend = 61200;
//Hour = 1 hour
    $hour = 1800;


    switch (strtoupper($mode)) {
        default:
            ?>
            <article class="container">
                <section class="row">
                    <div class="col-lg-6">
                        <?php
                        $calendar->PrintCalendar();
                        ?>
                    </div>
                    <div class="col-lg-6">
                        <h3>Dine bookings</h3>
                        <table class="table table-responsive table-striped table-hover">
                            <?php
                            $data = $calendar->GetBookings($auth->iUserID);
                            ?>
                            <tr>
                                <th>Booking Tidspunkt</th>
                                <th>Booking Kommentar</th>
                                <th>Fjern Booking</th>
                            </tr>
                            <?php

                            foreach ($data
                                     as $key => $value) {
                                ?>
                                <tr>
                                    <td><?php echo date("d - M  - H:i", $value["iBookingTime"]) ?></td>
                                    <td><?php echo $value["vcBookingName"] ?></td>
                                    <td><p data-placement="top" data-toggle="tooltip" title="Delete">
                                            <a href="?mode=DELETE&id='<?php echo $value["iBookingiD"] ?>' " class="btn btn-danger btn-xs"><span class="fa fa-trash"></span></a>
                                        </p></td>
                                </tr>
                                <?php
                            }
                            ?>
                        </table>
                    </div>
                </section>
            </article>
            <?php

            break;
        case "BOOKING":
            echo "<article class='container'>";
            echo "<section class='row'>";
            echo "<div class='col - lg - 12'>";
            echo "<h3>" . date("D - d - M - y", $s) . "</h3>";
            // Times

            // Times
            /**
             * $i < is number of rows
             */

            $rows = array_column($calendar->CheckifSigned(), "iBookingTime");


            echo "<form method='post'>";
            echo "<input name='mode' type='hidden' value='SAVE'>";
            echo "<select name='iBookingTime' class='form - control'>"; // border=\"1\" for visible border

            /**
             * Current Time is used for so you cant book dates in the past
             */
            $currentTime = ($s + 86400);
            if (time() < $currentTime) {
                /**
                 * by standard we say $i = $s + $daystart, which means we take $s which is the current date at 00:00 and we add on the daystart (8hours)
                 * i then say that $i is = or lower than $s + $dayend, which means its either equeal or lower than 17:00 in the afternoon
                 * Then i say that it needs to + an hour untill it hits $dayend
                 */
                for ($i = ($s + $daystart); $i <= ($s + $dayend); $i += $hour) { // FIXME for more entries or other step width
                    /**
                     * This echo's the selectbox options and if the timestamp from $rows is in the array from $i its gonna get the proberty "Disabled"
                     * so that it cannot be booked cause its already booked
                     */
                    echo '<option ' . (in_array($i, $rows) ? " Disabled " : "") . 'value="' . $i . '">' . date("H:i", $i) . '</option>';
                }
            }
            echo "</select>";
            echo "<textarea name='comment' class='form-control'></textarea>";
            echo " <button>Save my Signup</button></form>";
            echo "</div>";
            echo "</section>";
            echo "</article>";

            break;
        case
        "SAVE":
            /**
             * Current Time is used for so you cant book dates in the past
             */
            $currentTime = ($s + 86400);
            if (time() < $currentTime) {
                /**
                 * Runs $calendar->save
                 * Which just insert everything into the DB
                 */
                $calendar->save($auth->iUserID);
                header("location: ?mode=default");
            } else {
                echo "Kan ikke bookes";
            }
            break;

        case "DELETE";
            /**
             * Current Time is used for so you cant book dates in the past
             */
            $currentTime = ($s + 86400);
            if (time() < $currentTime) {
                /**
                 * Runs $calendar->Delete
                 * Which Completely drops the booking from the DB DOESNT SAVE IT
                 * If you want it to be kept in the db but deleted from the side you need a "iDeleted" field
                 * and only make the fields with "iDeleted = 0" appear and have effect on the page
                 */
                $calendar->Delete($_GET["id"]);
                header("location: ?mode=default");
            } else {
                echo "Kan ikke Fjernes";
            }

            break;
    }
    sysFooter();
} else {
    header("location: login.php");
}
?>
