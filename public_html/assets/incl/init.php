<?php
/*
 * Author @ Mads Roloff - Roloff-design
 */


//EasyPHP
define("DOCROOT", filter_input(INPUT_SERVER, "DOCUMENT_ROOT", FILTER_SANITIZE_STRING));
define("COREPATH", substr(DOCROOT, 0, strrpos(DOCROOT, "/")) . "/core/");


//Functions
require_once($_SERVER["DOCUMENT_ROOT"] . '/../core/functions.php');
/*ClassLoader*/
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . '/../core/classes/ClassLoader.php';
$Classloader = new ClassLoader();
//$db = new db();
$db = new dbconf();
$setup = new Setup();

$imageSel = new image();

$auth = new auth();
$auth->iShowLoginForm = 0;
$auth->authentificate();