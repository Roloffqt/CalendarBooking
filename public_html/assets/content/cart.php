<?php
/**
 * Created by PhpStorm.
 * Auther @ Mads Roloff - Rights reservede to Author
 * Date: 03-05-2017
 */ ?>

<?php
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/init.php";
sysHeader();
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/nav.php";

$iCartLineID = filter_input(INPUT_GET, "iCartLineID", FILTER_SANITIZE_NUMBER_INT, getDefaultValue(-1));

$mode = setMode();

switch (strtoupper($mode)) {


    default:


        $cart = new shopcart();
        $item = $cart->getItemByUser($auth->iUserID);
        $rows = $cart->getCartLines($item);
        $GrandTotal = $cart->getGrandTotal();


        if ($rows > null) {
            ?>

            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-10 col-md-offset-1">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Product</th>
                                <th>Quantity</th>
                                <th class="text-center">Price</th>
                                <th class="text-center">Total</th>
                                <th> </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($rows as $key => $row):
                                $product = new shopproduct();
                                $product->getItem($row["iProductID"]);
                                ?>
                                <tr id="cartline<?php echo $row["iCartLineID"]; ?>">
                                    <td class="col-sm-8 col-md-6">
                                        <div class="media">
                                            <a class="thumbnail pull-left" href="#">
                                                <img class="media-object" src="http://icons.iconarchive.com/icons/custom-icon-design/flatastic-2/72/product-icon.png" style="width: 72px; height: 72px;">
                                            </a>
                                            <div class="media-body">
                                                <h4 class="media-heading"><a href="#"><?php echo $product->vcTitle ?></a></h4>
                                                <h5 class="media-heading"> by <a href="#"><?php echo $product->vcTitle ?></a></h5>
                                                <span>Status: </span><span class="text-success"><strong><?php
                                                        if ($product->iStock > 0) {
                                                            echo "Varen er på lager";
                                                        } else {
                                                            "Varen er udsolgt";
                                                        } ?>
                                                </strong></span>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="col-sm-5 col-md-1" style="text-align: center">
                                        <input type="number" class="form-control" id="quantity-<?php echo $row["iCartLineID"]; ?>" value="<?php echo $row["iQuantity"] ?>">
                                        <button type="button" data-id="<?php echo $row["iCartLineID"]; ?>" class="update btn btn-success">
                                            <span class="fa fa-undo"></span> Antal
                                        </button>
                                    </td>

                                    <td class="col-sm-1 col-md-1 text-center"><strong class="price"><?php echo $product->iPrice; ?>,-</strong></td>
                                    <td class="col-sm-1 col-md-1 text-center"><strong class="linetotal"><?php echo $product->iPrice * $row["iQuantity"]; ?>,-</strong></td>
                                    <td class="col-sm-1 col-md-1">
                                        <button data-productid="<?php echo $row["iProductID"]; ?>" type="button" class="remove btn btn-danger">
                                            <span class="glyphicon glyphicon-remove"></span> Remove
                                        </button>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            <tr>
                                <td>  </td>
                                <td>  </td>
                                <td>  </td>
                                <td><h5>Subtotal</h5></td>
                                <td class="text-right total-sum"><h5><strong><?php echo $GrandTotal ?>,-</strong></h5></td>
                            </tr>
                            <tr>
                                <td>  </td>
                                <td>  </td>
                                <td>  </td>
                                <td><h5>Estimated shipping</h5></td>
                                <td class="text-right"><h5><strong><?php echo $shipping = 50; ?>,-</strong></h5></td>
                            </tr>
                            <tr>
                                <td>  </td>
                                <td>  </td>
                                <td>  </td>
                                <td><h3>Total</h3></td>
                                <td class="text-right "><h3><strong class="total-sum"><?php echo $GrandTotal + $shipping ?>,-</strong></h3></td>
                            </tr>
                            <tr>
                                <td>  </td>
                                <td>  </td>
                                <td>  </td>
                                <td>
                                    <button type="button" class="btn btn-default">
                                        <span class="glyphicon glyphicon-shopping-cart"></span> Continue Shopping
                                    </button>
                                </td>
                                <td>
                                    <a href="?mode=checkout">
                                        <button type="button" class="btn btn-success">
                                            Checkout <span class=" glyphicon glyphicon-play"></span>
                                        </button>
                                    </a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <?php
        } else { ?>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-10 col-md-offset-1">

                        <h2 style="text-align: center;"> Ingen vare i kurven! </h2>
                        <h4 style="text-align: center;"><a href="/index.php">Tilbage til Shoppen!</a></h4>


                    </div>
                </div>
            </div>

        <?php }
        sysFooter();
        break;
    case "CHECKOUT":

        //Logged in
        if ($auth->user->extranet) {
            ?>
            <article class="container">
                <section class="row">
                    <form>
                        <div class="col-lg-6">

                            <h2>Billing Address</h2>
                            <div class="form-group">
                                <label for="vcFirstName">Fornavn</label>
                                <input class="form-control" name="vcFirstName" data-requried="1" id="vcFirstName" type="text" value="<?php echo $auth->user->vcFirstName ?>">
                            </div>
                            <div class="form-group">
                                <label for="vcLastName">Efternavn</label>
                                <input class="form-control" name="vcLastName" data-requried="1" id="vcLastName" type="text" value="<?php echo $auth->user->vcLastName ?>">
                            </div>
                            <div class="form-group">
                                <label for="vcEmail">Email</label>
                                <input class="form-control" name="vcEmail" data-requried="1" id="vcEmail" type="text" value="<?php echo $auth->user->vcEmail ?>">
                            </div>
                            <div class="form-group">
                                <label for="vcAddress">Addresse</label>
                                <input class="form-control" name="vcAddress" data-requried="1" id="vcAddress" type="text" value="<?php echo $auth->user->vcAddress ?>">
                            </div>
                            <div class="form-group">
                                <label for="vcCity">By</label>
                                <input class="form-control" name="vcCity" data-requried="1" id="vcCity" type="text" value="<?php echo $auth->user->vcCity ?>">
                            </div>
                            <div class="form-group">
                                <label for="iZip">Postnummer</label>
                                <input class="form-control" name="iZip" data-requried="1" data-validate="Zip" id="iZip" type="text" value="<?php echo $auth->user->iZip ?>">
                            </div>

                        </div>
                        <div class="col-lg-6">

                            <h2>Shipping Address</h2>
                            <div class="form-group">
                                <input style="display: inline" checked type="checkbox" name="ShippingAddress" id="ShippingAddress">
                                <p style="display: inline">Brug billing addresse til shipping</p>
                            </div>
                            <div class="form-group">
                                <label for="vcDeAddress">Address</label>
                                <input class="form-control" disabled data-requried="1" name="vcDeAddress" id="vcDeAddress" type="text">
                            </div>
                            <div class="form-group">
                                <label for="vcDeCity">By</label>
                                <input class="form-control" disabled data-requried="1" name="vcDeCity" id="vcDeCity" type="text">
                            </div>
                            <div class="form-group">
                                <label for="iDeZip">Postnummer</label>
                                <input class="form-control" disabled data-requried="1" name="iDeZip" id="iDeZip" type="text" data-validate="Zip">
                            </div>
                            <div class="form-group">
                                <input style="display: inline" data-requried="1" type="checkbox" name="ShippingAddress" id="ShippingAddress">
                                <p style="display: inline">Jeg har læst og accepteret vilkår og betingelser og accepterer politikken for beskyttelse af private oplysninger</p>
                            </div>

                            <button type="button" onclick="validate(this.form)" class="btn btn-success"><i class="fa fa-check">Bestil</i></button>
                            <button type="button" onclick="" class="btn btn-brand"><i class="fa fa-check">Shop Videre</i></button>

                        </div>
                    </form>
                </section>
            </article>
            <?php
        }

        //Not logged in
        if (!$auth->user->extranet) {
            ?>
            <article class="container">
                <section class="row">
                    <div class="col-lg-offset-4 col-lg-4">
                        <h2>Login for at forsætte </h2>
                        <form id="loginform" method="post" autocomplete="off">

                            <div class="form-group">
                                <label for="username" class=""> Username</label>
                                <input type="text" class="form-control" id="username" autocapitalize="none" name="username" placeholder="Indtast brugernavn" value="">
                            </div>

                            <div class="form-group">
                                <label for="password" class=""> Password</label>
                                <input type="password" class="form-control" id="password" name="password" placeholder="password" value="">
                            </div>

                            <div class="btn-group">
                                <button type="submit" value="login" class="login-button"><i class="fa fa-chevron-right">Login</i></button>

                            </div>

                            <a href="login.php"><p>Ny kunde? - Opret bruger</p></a>
                        </form>
                    </div>
                </section>
            </article>
            <?php
        }
        break;

}
sysFooter();