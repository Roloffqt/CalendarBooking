<?php
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/init.php";
sysHeader();
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/nav.php";
$mode = setMode();
if ($auth->user->extranet) {
    switch (strtoupper($mode)) {

        Default:

            ?>

            <div class="container">
                <?php
                $event = new event();
                $rows = $event->GetSelect(6);
                $event->key = filter_input(INPUT_POST, "iEventID", FILTER_VALIDATE_INT);

                $accHtml = "<div style='padding:20px !important; margin: 0 !important;' class=\"row\">\n";
                foreach ($rows as $key => $row) {
                    $accHtml .= "<a href='?mode=DETAILS&iEventID=" . $row["iEventID"] . "'>";
                    $accHtml .= "<div style='padding:20px !important;' class=\"col-lg-4 event-subpage\">\n";
                    $accHtml .= "<div style='padding: 15px !important;' class=\"col-lg-12 red-box redbox-height event-desc\">\n";
                    $accHtml .= "<div class='col-lg-12'><img style=' max-width:100%;' src='" . $row["vcImage"] . "'></div>\n";

                    $accHtml .= "<div class='col-lg-9'><p class=\"redbox-font\">" . $row["vcTitle"] . "</p></div>\n";
                    $accHtml .= "<div class='col-lg-3'><p class=\"redbox-desc\">" . date('d', $row['daCreated']) . "th<br/><span style='text-transform: uppercase;'>" . date('M', $row['daCreated']) . "</span></p>\n</div>\n";
                    $accHtml .= "</div>\n";
                    $accHtml .= "</div>\n";
                    $accHtml .= "</a>";
                }

                $accHtml .= "</div>\n";
                echo $accHtml;

                ?>
            </div>

            <?php
            sysFooter();
            break;
        case "DETAILS":
            ?>
            <?php
            $event = new event();
            $Signup = new Signup();

            $iEventID = filter_input(INPUT_GET, "iEventID", FILTER_VALIDATE_INT);
            $row = $event->getEvent($iEventID);

            $totalsignups = $Signup->GetSum();
            $usertotal = $Signup->GetMaxNum($auth->iUserID);

            //Makes the "amount of spots" left after some signups the max for the input field
            $maxNumber = ($row[0]["iMaxSignUps"] - $totalsignups) + $usertotal;

            $rows = $Signup->GetDetails($auth->iUserID);

            if ($totalsignups > $row[0]["iMaxSignUps"]) {
                echo "<h1>Sorry But max signups has been reached</h1>";
            } else {
                if (filter_input(INPUT_POST, "signups", FILTER_SANITIZE_NUMBER_INT)) {
                    //Insert statment in class Signup
                    $Signup->MakeSignup($auth->iUserID, $iEventID);
                    header("location: ?mode=DETAILS&iEventID=$iEventID");
                }
            }
            ?>
            <!-- Display details about event! -->
            <div class="container">
            <h1><?php echo $row[0]["vcTitle"] ?></h1>
            <div><?php echo $row[0]["vcShortDesc"] ?></div>

            <div><?php echo $row[0]["txLongDesc"] ?></div>
            <img src="<?php echo $row[0]["vcImage"] ?>">
            <h3><?php echo $row[0]["vcPlace"] ?></h3>
            <h3><?php echo date('d,m,y', $row[0]['daStartDate']) ?></h3>
            <h3><?php echo date('d,m,y', $row[0]['daExpireDate']) ?></h3>

            <h3>Total Signups<span style="font-weight: bolder;">
                    <?php echo $totalsignups; ?>
                </span>/<span style="font-weight: bolder;">
                    <?php echo $row[0]["iMaxSignUps"] ?>
                </span></h3>

            <?php
            if ($auth->user->extranet || $auth->user->sysadmin || $auth->user->admin) {
                if ($rows > NULL) { ?>
                    <!--Delete entry!-->
                    <form METHOD="post">
                        <input name='mode' type='hidden' value='DELETE'>
                        <button>Delete my Signup</button>
                    </form>
                <?php } ?>

                <!-- Make/update entry! -->
                <form method="post">
                    <input type="number" max="<?php echo $maxNumber ?>" name="signups" value="<?php echo $rows[0]["iSignUps"] ?>" data-required="1">

                    <?php if ($rows > NULL) { ?>
                        <h4>Du er allerede tilmeldt med med <span style="font-weight: bolder;"><?php echo $rows[0]["iSignUps"] ?></span> Tilmeldinger, du kan rette til her</h4>
                        <label>Note
                            <textarea name="note" class="form-control"><?php echo $rows[0]["vcNote"] ?></textarea>
                        </label>
                        <button> update stuff</button>

                    <?php } else { ?>
                        <label>Note
                            <textarea name="note" class="form-control"></textarea>
                        </label>
                        <button> Book stuff</button>
                    <?php } ?>
                </form>
                </div>
                <?php
            } else {
                echo "<h2>Du skal være logget ind for at booke!</h2>";
            }
            break;
        case
        "DELETE":
            $Signup = new Signup();
            $iEventID = filter_input(INPUT_GET, "iEventID", FILTER_VALIDATE_INT);
            $Signup->Delete($auth->iUserID);
            header("location: ?mode=DETAILS&iEventID=$iEventID");
            break;
    }
} else {
    header("location: login.php");
}
?>